/**
 * @fileoverview This demo is used for MarkerClusterer. It will show 100 markers
 * using MarkerClusterer and count the time to show the difference between using
 * MarkerClusterer and without MarkerClusterer.
 * @author Luke Mahe (v2 author: Xiaoxi Wu)
 */

function $(element) {
  return document.getElementById(element);
}

var speedTest = {};

speedTest.pics = null;
speedTest.map = null;
speedTest.markerClusterer = null;
speedTest.markers = [];
speedTest.infoBox = null;

speedTest.init = function() {
  var latlng = new google.maps.LatLng(-23.890439, -46.707068);
  var options = {
    zoom: 13,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    navigationControl: false,
    scrollwheel: false,
    streetViewControl: false,
    mapTypeControl: false,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };

  speedTest.map = new google.maps.Map(document.getElementById('map'), options);
  speedTest.pics = data.photos;
  speedTest.infoBox = new InfoBox();

  speedTest.showMarkers();
};

speedTest.showMarkers = function() {
  speedTest.markers = [];

  if (speedTest.markerClusterer) {
    speedTest.markerClusterer.clearMarkers();
  }

  var numMarkers = data.count;

  for (var i = 0; i < numMarkers; i++) {

    var latLng = new google.maps.LatLng(speedTest.pics[i].latitude,
        speedTest.pics[i].longitude);

    var marker = new google.maps.Marker({
      'position': latLng
    });

    var fn = speedTest.markerClickFunction(speedTest.pics[i], latLng, marker);
    google.maps.event.addListener(marker, 'click', fn);

    if (i === 0) { // Abrir o primeiro box para informações gerais
      google.maps.event.trigger(marker, 'click', {
        latLng: new google.maps.LatLng(0, 0)
      });
    }

    speedTest.markers.push(marker);
  }
  
  for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
    marker.setMap(speedTest.map);
  }
};

speedTest.markerClickFunction = function(pic, latlng, marker) {
  return function(e) {
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) {
      e.stopPropagation();
      e.preventDefault();
    }
    var infoHtml = '';
    if (pic.photo_file_url == '') {
      infoHtml = 
        '<div class="card">' +
          '<div style="max-width: '+ pic.max_width +'px; padding: 20px;">' +
            '<span class="card-title activator grey-text text-darken-4">' + pic.photo_title +
            '<p style="font-size: 1rem;">' + pic.content + '</p>' +
          '</div>' +
        '</div>';
    } else {      
      infoHtml = 
        '<div class="card">' +
          '<div class="card-image waves-effect waves-block waves-light" style="max-width: '+ pic.max_width +'px;">' +
            '<a href="' + pic.photo_url + '"><img class="activator" src="' + pic.photo_file_url + '"/></a>' +
          '</div>' +
          '<div style="max-width: '+ pic.max_width +'px; padding: 20px;">' +
            '<span class="card-title activator grey-text text-darken-4">' + pic.photo_title +
            '<p style="font-size: 1rem;">' + pic.content + '</p>' +
          '</div>' +
        '</div>';
    }
  
    var myOptions = {
      content: infoHtml
      ,disableAutoPan: false
      ,maxWidth: 0
      ,pixelOffset: new google.maps.Size(-(pic.max_width/2), 0)
      ,zIndex: null 
      ,closeBoxMargin: "8px 2px 2px 2px"
      ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
      ,infoBoxClearance: new google.maps.Size(1, 1)
      ,isHidden: false
      ,pane: "floatPane"
      ,enableEventPropagation: true
    };

    speedTest.infoBox.close();
    speedTest.infoBox.setOptions(myOptions);
    speedTest.infoBox.open(speedTest.map, marker);
  };
};

speedTest.clear = function() {
  for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
    marker.setMap(null);
  }
};

speedTest.change = function() {
  speedTest.clear();
  speedTest.showMarkers();
};

speedTest.time = function() {
  $('timetaken').innerHTML = 'timing...';
  var start = new Date();
  if ($('usegmm').checked) {
    speedTest.markerClusterer = new MarkerClusterer(speedTest.map, speedTest.markers);
  } else {
    for (var i = 0, marker; marker = speedTest.markers[i]; i++) {
      marker.setMap(speedTest.map);
    }
  }

  var end = new Date();
  $('timetaken').innerHTML = end - start;
};
